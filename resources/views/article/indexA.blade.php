@extends('layout')         

@section('contenu')
            <div class="content">
                <div class="title m-b-md">
                  <h1>Mes articles </h1>  
                </div>
              
            </div>
            <div class="container" >
                @forelse ($articless as $article)
                <h3><strong>Talk about : {{ $article->title}}</strong></h3>
                <p>{{ $article->content}}</p>
                <a href="/article/{{ $article->id}}"><button type="button" class="btn btn-outline-primary">En savoir plus ..</button></a><br>
                <br><br>
                @empty
                <p>Sorry ! Pas de projet a vous montrer pour le moment </p>
                    
                @endforelse
            </div>
           
@endsection