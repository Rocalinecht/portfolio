
@extends('layout')         

@section('contenu')


<div class=" container flex-card"  style="margin-top: 5%">
    <h2>Gestion des articles</h2>
    <br>

  
    <a href="/article/create"><button type="submit" class="btn btn-outline-primary">Ajouter un nouvelle article</button></a>
</div>
<div class="container ">


    @foreach ($articles as $article)
  
    <div class="card">
     <div class="card-body flex-card">
       <p><span class="font-weight-bold">Titre :</span> {{ $article->title}}</p>
       <div class="div_button">
          <a href="/article/{{ $article->id}}/edit" ><button type="button" class="btn btn-outline-secondary">modifier</button></a>
       
        <form action="/article/{{ $article->id }}" method="POST">
          @method('DELETE')
          @csrf
          <button type="submit" class="btn btn-outline-danger">Supprimer</button>
      
      </form> 
       </div>
     </div>
   </div>
   @endforeach
   
  </div> 

@endsection