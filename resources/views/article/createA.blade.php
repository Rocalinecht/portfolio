@extends('layout')         

@section('contenu')
    <div class="container">
        <div class="title m-b-md">
            <h2>Ajouter un  article </h2>
        </div>  
        <form action="/article" method="POST">
            @csrf   
            <div class="form-group">
                <label for="title" class="font-weight-bold">Titre de l'article</label>
                <input type="text" name="title" class="form-control"  value=" {{ old('title')}}">
                @error('title') <p style="color:red">{{ $message }}</p>@enderror
            </div>
            <div class="form-group">
                <label for="content" class="font-weight-bold"> Contenue de l'article</label>
                <textarea class="form-control" name="content" rows="3"> {{ old('content')}}</textarea>
                @error('content') <p style="color:red">{{ $message }}</p>@enderror
            </div>
            
            <div class="form-group">
                <label for="img_url" class="font-weight-bold">Lien URL de l'image</label>
                <input type="url" name="img_url" class="form-control"  value=" {{ old('img_url')}}" >
                @error('img_url') <p style="color:red">{{ $message }}</p>@enderror
            </div>
              <button type="submit" class="btn btn-primary">Ajouter </button>
            </form>
            <a href="/adminpage/articles">Annuler</a>
 
    </div>
@endsection