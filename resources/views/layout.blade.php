<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
html, body {
    background-color: #fff;
    color: #636b6f;
    font-family: 'Nunito', sans-serif;
    font-weight: 200;
    height: 100vh;
    margin: 0;
}

.full-height {
    height: 100vh;
}

.flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
    flex-direction: column;
}

.position-ref {
    position: relative;
}

.top-right {
    position: absolute;
    right: 10px;
    top: 18px;
}

.content, h2 {
    text-align: center;
    
}

 h1 {
    font-size: 84px;
}
h2{
  margin-top: 50px
}
.adminNavbar{
  margin-top: 0px !important;
  height: ;
  position: sticky;
  margin-bottom: 100px;

}
.adminpage{
  margin: 10%;
}

.m-b-md {
   margin: 10%
}
.page-footer {
    background-color: aliceblue;
    margin-top: 100px;
}
.veille{
    align-items: center;
    display: flex;
    justify-content: space-between;
}
.flex-card{
  display: flex;
  justify-content: space-between;
}
.div_button{
  display: flex;
  
}



















        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
              <a class="navbar-brand" href="#">Portfolio</a>
              <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                  <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="/aboutme">About me <span class="sr-only"></span></a>
                  </li>
                <li class="nav-item">
                  <a class="nav-link" href="/projet">Projet</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/article">Article</a>
                  </li>
              </ul>
              <div class="form-inline my-2 my-lg-0">
                @if(auth()->check())
                <a href="/adminpage"><button type="submit" class="btn btn-outline-secondary" style="margin-right: 15px">Admin page</button></a> 
                <a href="/deconnexion"><button type="submit" class="btn btn-secondary">deconnexion</button> </a>
                @else
                <a href="/connexion"><button type="submit" class="btn btn-outline-secondary">Admin</button> </a>
                @endif
                 
              </div>
            </div>
          </nav>





        <div class=""">
            @yield('contenu')  
        </div>

    <!-- Footer -->
<footer class="page-footer ">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3 full-width">© 2020 Copyright: Caroline Chatelon
      
    </div>
    <!-- Copyright -->
  
  </footer>
  <!-- Footer -->

</body>
</html>