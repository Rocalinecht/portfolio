@extends('layout')         

@section('contenu')
<ul class="nav justify-content-center "style="background-color: #e3f2fd;">
  <li class="nav-item">
    <a class="nav-link active" href="/addcategory">Ajouter une categorie</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/projets/create">Ajouter un projet</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/addarticle">Ajouter un article</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/" >Home page</a>
  </li>
</ul>

<div class="flex-center position-ref adminpage">
    <h1>PAGE ADMINISTRATEUR</h1>
</div>
<div class="container ">
  <a href="/adminpage/articles" ><button type="button" class="btn btn-lg btn-outline-secondary">Mes articles</button></a>

  <a href="/adminpage/projets" ><button type="button" class="btn btn-lg btn-outline-secondary">Mes projets</button></a>
  <a href="#" ><button type="button" class="btn btn-secondary btn-lg btn-outline disabled">Mes messages</button></a>

</div>


</div> 

</div>

@endsection