@extends('layout')         

@section('contenu')
          <h2>Ajouter une nouvelle categorie au projet </h2>
          <form action="/addcategory" method="POST">
            @csrf   
            <div class="form-group">
                <label for="name" class="font-weight-bold">Nom categorie</label>
               <p><input type="text" name="namecategorie" class="form-control" ></p> 
            </div>
            <div class="form-group">
                <label for="slug" class="font-weight-bold">Slug souhaiter</label>
                <input type="text" name="slug" class="form-control" placeholder="/ceci-est-un-slug">
            </div>
              <input type="submit" value="ajouter">
          </form>
@endsection
