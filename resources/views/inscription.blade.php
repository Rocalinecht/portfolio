@extends('layout')         

@section('contenu')
            <div class="content">
                <form action="" method="POST">
                    @csrf   

                <p><input type="text" name="lastname" placeholder="votre nom" value="{{ old('lastname')  }}"></p>
                    
                    <p><input type="text" name="firstname" placeholder="votre prénom" value="{{ old('firstname')  }}"></p>
                   
                    <p><input type="email" name="email" placeholder="votre email" value="{{ old('email')  }}"></p>
                    @if($errors->has('email'))
                        <p>{{ $errors->first('email')}}</p>
                    @endif
                    <p><input type="password" name="password" placeholder="créer votre mot de passe" ></p>
                    @if($errors->has('password'))
                        <p>{{ $errors->first('password')}}</p>
                    @endif
                    <p><input type="password" name="password_confirmation" placeholder="confirmer votre mot de passe"></p>
                    @if($errors->has('password_confirmation'))
                        <p>{{ $errors->first('password_confirmation')}}</p>
                    @endif
                    <p><input type="text" name="img_url" placeholder="lien de votre avatar" value="https://i.picsum.photos/id/1/200/200.jpg"></p>
                    
                    <p><input type="phone" name="phone" placeholder="votre numero de telephone" value="{{ old('phone') }}"></p>

                    <p><input type="text" name="adresse" placeholder="votre adresse" value="{{ old('adresse')  }}"></p>

                    {{-- <p><input type="submit" value="M'inscrire"></p> --}}
                    <button type="button" class="btn btn-secondary">M'inscrire</button>
                </form>
            </div>
@endsection
