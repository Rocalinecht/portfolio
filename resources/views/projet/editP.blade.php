@extends('layout')         

@section('contenu')

<div class="container">
        <div class="title m-b-md">
            <h2>Ajouter un nouveau projet </h2>
        </div>  
    <form action="/projets/{{ $projet->id}}" method="POST">
              @method('PATCH')
            @csrf   
            <div class="form-group">
                <label for="nameProjet" class="font-weight-bold">Nom du projet</label>
                <input type="text" name="nameProjet" class="form-control"  value=" {{ $projet->nameProjet }}" >
            @error('name') <p style="color:red">{{ $message }}</p>@enderror
            </div>
            <div class="form-group">
                <label for="description" class="font-weight-bold">Description</label>
                <textarea class="form-control" name="description" rows="5" value="">{{ $projet->description}}</textarea>
                @error('description') <p style="color:red">{{ $message }}</p>@enderror
            </div>
            <div class="form-group">
                <label for="img_url" class="font-weight-bold">Image </label>
                <input type="text" class="form-control" name="img_url"  value=" {{ $projet->img_url }}">
                @error('img_url') <p style="color:red">{{ $message }}</p>@enderror
            </div>
            <div class="form-group" >
                <label for="technology" class="font-weight-bold">Technologie utilisé</label>
                <input type="text" name="technology" class="form-control" value="{{ $projet->technology }}" >
                @error('technologie') <p>{{ $message }}</p>@enderror
            </div>
            <div class="form-group">
                <label for="repot_url" class="font-weight-bold">Lien repot Gitlab</label>
                <input type="text" name="repot_url" class="form-control" value="{{ $projet->repot_url }}" >
                @error('repot_url') <p style="color:red">{{ $message }}</p>@enderror
            </div>
            <div class="form-group">
                <label for="web_url" class="font-weight-bold">Lien du site </label>
                <input type="text" name="web_url" class="form-control" value="{{ $projet->web_url }}" >
            </div>
            <div class="form-group">
                <label for="category_id" class="font-weight-bold">Catégorie </label>
                <input type="text" name='category_id' class="form-control" value="{{ $projet->category_id }}">
                @error('category') <p style="color:red">{{ $message }}</p>@enderror
              
              </div>
              <button type="submit" class="btn btn-primary">Modifier </button>
          </form>
        </div>
@endsection