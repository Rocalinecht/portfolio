@extends('layout')         

@section('contenu')

<div class="container">
        <div class="title m-b-md">
            <h2>Ajouter un nouveau projet </h2>
        </div>  
          <form action="/projets" method="POST">
            @csrf   
            <div class="form-group">
                <label for="nameProjet" class="font-weight-bold">Nom du projet</label>
                <input type="text" name="nameProjet" class="form-control"  value=" {{ old('name')}}" >
            @error('name') <p style="color:red">{{ $message }}</p>@enderror
            </div>
            <div class="form-group">
                <label for="description" class="font-weight-bold">Description</label>
                <textarea class="form-control" name="description" rows="3" value=" {{ old('description')}}"></textarea>
                @error('description') <p style="color:red">{{ $message }}</p>@enderror
            </div>
            <div class="form-group">
                <label for="img_url" class="font-weight-bold">Image </label>
                <input type="file" class="form-control-file" name="img_url"  value=" {{ old('img_url')}}">
                @error('img_url') <p style="color:red">{{ $message }}</p>@enderror
            </div>
            <div class="form-group" >
                <label for="technology" class="font-weight-bold">Technologie utilisé</label>
                <input type="text" name="technology" class="form-control" value=" {{ old('technologie')}}" >
                @error('technologie') <p>{{ $message }}</p>@enderror
            </div>
            <div class="form-group">
                <label for="repot_url" class="font-weight-bold">Lien repot Gitlab</label>
                <input type="text" name="repot_url" class="form-control" value=" {{ old('repot_url')}}" >
                @error('repot_url') <p style="color:red">{{ $message }}</p>@enderror
            </div>
            <div class="form-group">
                <label for="web_url" class="font-weight-bold">Lien du site </label>
                <input type="text" name="web_url" class="form-control" value=" {{ old('website_url')}}" >
            </div>
            <div class="form-group">
                <label for="category_id" class="font-weight-bold">Catégorie </label>
                <input type="text" name='category_id' class="form-control" value=" {{ old('category')}}">
                @error('category') <p style="color:red">{{ $message }}</p>@enderror
                
              </div>
              <button type="submit" class="btn btn-primary">Ajouter </button>
            </form>
            <a href="/adminpage/projets">Annuler</a>
        </div>
@endsection