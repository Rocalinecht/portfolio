
@extends('layout')         

@section('contenu')


<div class=" container flex-card">
    <h3>Les projets : </h3>
  
    <a href="/projets/create"><button type="submit" class="btn btn-outline-primary">Ajouter un nouveau projet</button></a>
  </div>
   <div class="container " style="margin-top: 5%">
  
     @foreach ($projets as $projet)
  
     <div class="card">
      <div class="card-body flex-card">
        <p><span class="font-weight-bold">Titre :</span> {{ $projet->nameProjet}}</p>
        <div class="div_button">
          <a href="/projets/{{ $projet->id}}/edit" ><button type="button" class="btn btn-outline-secondary">modifier</button></a>
        
          <form action="/projets/{{ $projet->id }}" method="POST">
           @method('DELETE')
           @csrf
           <button type="submit" class="btn btn-outline-danger">Supprimer</button>
       
       </form>
        </div>
      </div>
    </div>
    @endforeach
    
  </div>




@endsection