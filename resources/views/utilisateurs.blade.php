@extends('layout')         

@section('contenu')
            <div>
                <h2>Les utilisateurs : </h2>
                <ul>
                    @foreach ($utilisateurs as $utilisateur)
                        <li>{{ $utilisateur->lastname}} {{ $utilisateur->firstname}}</li>
                    @endforeach
                </ul>
            </div>
@endsection
