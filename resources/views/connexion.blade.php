@extends('layout')         

@section('contenu')
            <div class="content">
                <form action="/connexion" method="POST">
                    @csrf   
                    <div class="form-group">
                        <label for="email">Adresse email </label>
                        <input  class="form-control" type="email" name="email"  value="{{ old('email')  }}" placeholder="Enter email">
                        @if($errors->has('email'))
                          <p>{{ $errors->first('email')}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password">Mot de passe</label>
                        <input class="form-control" type="password" name="password" placeholder="Entrée votre mot de passe">
                        @if($errors->has('password'))
                            <p>{{ $errors->first('password')}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Mot de passe (confirmation)</label>
                        <input class="form-control" type="password" name="password_confrimation" placeholder="Confrimer votre mot de passe">
                        @if($errors->has('password_confirmation'))
                            <p>{{ $errors->first('password_confirmation')}}</p>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Connexion</button> 
                </form>
            </div>
@endsection
