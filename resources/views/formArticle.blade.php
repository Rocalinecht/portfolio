@extends('layout')         

@section('contenu')
    <div class="container">
        <div class="title m-b-md">
            <h2>Ajouter un  article </h2>
        </div>  
        <form action="/addarticle" method="POST">
            @csrf   
            <div class="form-group">
                <label for="titre" class="font-weight-bold">Titre de l'article</label>
                <input type="text" name="titre" class="form-control" >
            </div>
            <div class="form-group">
                <label for="contenue" class="font-weight-bold"> Contenue de l'article</label>
                <textarea class="form-control" name="contenue" rows="3"></textarea>
            </div>
            
            <div class="form-group">
                <label for="imgblog_url" class="font-weight-bold">Lien URL de l'image</label>
                <input type="url" name="imgblog_url" class="form-control" >
            </div>
              <button type="submit" class="btn btn-primary">Ajouter </button>
        </form>
 
    </div>
@endsection