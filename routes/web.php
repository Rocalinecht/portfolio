<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// ----> Route simple page 
Route::view('/','welcome');
Route::view('/aboutme','aboutme');
Route::view('/adminpage','adminPage');

// ----> Route inscrpition
Route::get('/inscription', 'InscriptionController@showFormulaire');
Route::post('/inscription', 'InscriptionController@validateFormulaire');
Route::get('/utilisateurs', 'UtilisateursController@liste');

// ----> Route connexion
Route::get('/connexion', 'ConnexionController@showConnexionForm');
Route::post('/connexion', 'ConnexionController@validateConnexion');

// -----> Page admin 
Route::get('/adminpage', 'CompteController@accueil'); 
Route::get('/deconnexion', 'CompteController@deconnexion'); 

// Route::get('/adminpage', 'ArticlesController@indexAdmin');

// ----> Categorie
 Route::get('/addcategory', 'CategoriesController@formcategorie'); 
 Route::post('/addcategory', 'CategoriesController@newcategorie');


// --->Projet
Route::get('/projets', 'ProjetsController@index');
Route::get('/adminpage/projets', 'ProjetsController@projetAdmin');
Route::get('/projets/create', 'ProjetsController@create');
Route::post('/projets', 'ProjetsController@store');
Route::get('/projets/{projet}', 'ProjetsController@show');
Route::get('/projets/{projet}/edit', 'ProjetsController@edit');
Route::patch('/projets/{projet}', 'ProjetsController@update');
Route::delete('/projets/{projet}', 'ProjetsController@destroy');

// -----> Article
Route::get('/article', 'ArticlesController@index');
Route::get('/adminpage/articles', 'ArticlesController@indexAdmin');
Route::get('/article/create', 'ArticlesController@create');
Route::post('/article', 'ArticlesController@store');
Route::get('/article/{article}', 'ArticlesController@show');
Route::get('/article/{article}/edit', 'ArticlesController@edit');
Route::patch('/article/{article}', 'ArticlesController@update');
Route::delete('/article/{article}', 'ArticlesController@destroy');









// Route::get('/addarticle', function(){
//     return view('formArticle');
// }); 

// Route::get('/addarticle', 'ArticlesController@article');
// Route::post('/addarticle', 'ArticlesController@newArticle');
// Route::post('/addarticle', 'ArticlesController@deleteArticle');

// Route::get('/adminpage', "ArticlesController@listeArticleAdmin"); 
// Route::get('/article', "ArticlesController@listeArticleuser"); 
