<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projet extends Model
{
    protected $fillable =['nameProjet', "description", "img_url", "technology", "repot_url", "web_url","category_id", ];

    protected $guarded =[];

}
