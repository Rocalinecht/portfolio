<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    //--> Afficher les articles coté user
    public function index()
    {
        $articless = \App\Article::all();
        
        return view('article.indexA', compact('articless'));
    }

   // --> Afficher les articles coté admin
    public function indexAdmin()
    {
        $articles = \App\Article::all();
        
        return view('article.indexAdmin', compact('articles'));
    }

    //--> Afficher le formulaire d'ajout projet 
    public function create()
    {
        // if(auth()->guest()){
        //     return redirect('/connexion')->withErrors([
        //         'email' => ' vous devez être connecter pour accèder à cette page',
        //     ]);
        // }
        return view('article.createA');
    }
    public function store()
    {
        $data = request()->validate([
            'title' => 'required',
            'content' => 'required',
            'img_url' => 'required',
            
        ]);

        \App\Article::create($data);

        return redirect('/adminpage/articles');
    }

    //--> Afficher les detail d'un article sur une page 
    public function show(\App\Article $article)
    {
        // $projet = \App\Projet::findOrFail($projet);
        return view('article.showA', compact('article'));
    }

     //--> Editer un projet deja existant
     public function edit(\App\Article $article)
     {
         
         return view('article.editA', compact('article'));
     }

      //--> Envoyer les modification dans BDD
    public function update(\App\Article $article)
    {
        $data = request()->validate([
            'title' => 'required',
            'content' => 'required',
            'img_url' => 'required',
            
        ]);
        $article->update($data);

        return redirect('/adminpage/articles');

    }

    //--> Supprimer un projet
    public function destroy(\App\Article $article){

        $article->delete();

        return redirect('/adminpage/articles');

    }

}
