<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorie;


class CategoriesController extends Controller
{
    //
    public function listecategorie(){
        
        $categories = Categorie::all();
        return view('formProjet',[
            "categories" => $categories,
        ]);
    }

    public function formcategorie(){
        return view('categorie');
    }


    public function newcategorie(){

        //  $category = new App\Categorie;
    //  $category->name_category = request('namecategorie');
    //  $category->slug_url = request('slug');

    //  $category->save();

     $category = Categorie::create([
        'name_category' => request('namecategorie'),
        'slug_url' => request('slug'),
     ]);
        
        return redirect('/adminpage');
    }
}
