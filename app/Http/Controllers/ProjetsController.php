<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProjetsController extends Controller
{
    //--> Afficher les projet coté user
    public function index()
    {
        $projetss = \App\Projet::all();
        
        return view('projet.indexP', compact('projetss'));
    }

    //--> Afficher les projet coté admin
    public function projetAdmin()
    {
        $projets = \App\Projet::all();
        
        return view('projet.indexAdmin', compact('projets'));
    }

    //--> Afficher le formulaire d'ajout projet 
    public function create()
    {
        if(auth()->guest()){
            return redirect('/connexion')->withErrors([
                'email' => ' vous devez être connecter pour accèder à cette page',
            ]);
        }
        return view('projet.createP');
    }

    //--> Inserer nouveau projet dans la database
    public function store()
    {
        
        $data = request()->validate([
            'nameProjet' => 'required',
            'description' => 'required',
            'img_url' => 'required',
            'technology' => 'required',
            'repot_url' => 'required',
            'web_url' => 'required',
            'category_id'=> 'required',
        ]);

        \App\Projet::create($data);

        return redirect('/adminpage/projets');

    }

    //--> Afficher les detail d'un projet sur une page 
    public function show(\App\Projet $projet)
    {
        // $projet = \App\Projet::findOrFail($projet);
        return view('projet.showP', compact('projet'));
    }


    //--> Editer un projet deja existant
    public function edit(\App\Projet $projet)
    {
        
        return view('projet.editP', compact('projet'));
    }


    //--> Envoyer les modification dans BDD
    public function update(\App\Projet $projet)
    {
        
        $data = request()->validate([
            'nameProjet' => 'required',
            'description' => 'required',
            'img_url' => 'required',
            'technology' => 'required',
            'repot_url' => 'required',
            'web_url' => 'required',
            'category_id'=> 'required',
        ]);

        $projet->update($data);

        return redirect('/adminpage/projets');

    }

    //--> Supprimer un projet
    public function destroy(\App\Projet $projet){

        $projet->delete();

        return redirect('/adminpage/projets');

    }
}
