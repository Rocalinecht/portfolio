<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConnexionController extends Controller
{
    //
    public function showConnexionForm(){
        return view('connexion');
    }

    public function validateConnexion(){

        request()->validate([
            'email'=>['required', 'email'],
            'password' => ['required'],
        ]);

       $resultat = auth()->attempt([
            'email' => request('email'),
            'password' => request('password'),
        ]);

        var_dump($resultat);
        if($resultat){
            return redirect('/adminpage');
        }

        return back()->withInput()->withErrors([
            'email' => "Vos identifiants sont incorrects",
            'password' => "Mot de passe incorrects"
        ]);
    }
}
