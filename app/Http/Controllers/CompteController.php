<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CompteController extends Controller
{
    //
    public function accueil(){

        // var_dump(auth()->guest());

        if(auth()->guest()){
            return redirect('/connexion')->withErrors([
                'email' => ' vous devez être connecter pour accèder à cette page',
            ]);
        }

        return view('adminPage');
    }

    public function deconnexion(){
        auth()->logout();
        return redirect('/');
    }

}
