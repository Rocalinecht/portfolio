<?php

namespace App\Http\Controllers;

use App\Utilisateur;

class InscriptionController extends Controller
{
    //
    public function showFormulaire(){
        return view('inscription');
    }

    public function validateFormulaire(){
        request()->validate([
            'lastname' => ['required'],
            'firstname' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required','confirmed', 'min:8'],
            'password_confirmation' => ['required'],
            'img_url' => ['required'],
            'phone' => ['required'],
            'adresse' => ['required'],
        ]);
    
        $utilisateur = Utilisateur::create([
            'lastname'=>request('lastname'),
            'firstname'=>request('firstname'),
            'email'=>request('email'),
            'mot_de_passe'=>bcrypt(request('password')),
            'img_url'=>request('img_url'),
            'phone'=>request('phone'),
            'adresse'=>request('adresse'),
            
        ]);
    
    
        return ' Merci de votre inscription '.request('firstname');
    }
}
